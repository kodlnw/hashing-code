/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashingcode;

import java.util.LinkedHashSet;
import java.util.Iterator;

/**
 *
 * @author roman
 */
public class LinkedHashSet {

    public static void main(String args[]) {

        LinkedHashSet<String> set = new LinkedHashSet();
        set.add("Jin");
        set.add("Kazuya");
        set.add("Heihachi");
        set.add("Josie");
        set.add("Julia");
        Iterator<String> i = set.iterator();
        while (i.hasNext()) {
            System.out.println(i.next());
        }
    }
}
