/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashingcode;

import java.util.LinkedHashMap;

/**
 *
 * @author roman
 */
public class LinkedHashMap {

    public static void main(String a[]) {
        LinkedHashMap<String, String> lhm
                = new LinkedHashMap<String, String>();
        lhm.put("one", "Tanadon.Pongtuwanon");
        lhm.put("two", "Lnwza");
        lhm.put("three", "007");

        // It prints the elements in same order  
        // as they were inserted     
        System.out.println(lhm);

        System.out.println("Getting value for key 'one': "
                + lhm.get("one"));
        System.out.println("Size of the map: " + lhm.size());
        System.out.println("Is map empty? " + lhm.isEmpty());
        System.out.println("Contains key 'two'? "
                + lhm.containsKey("two"));
        System.out.println("Contains value 'Tanadon"
                + "Pongtuwanon'? " + lhm.containsValue("Tanadon"
                        + ".Pongtuwanon"));
        System.out.println("delete element 'one': "
                + lhm.remove("one"));
        System.out.println(lhm);
    }
}
