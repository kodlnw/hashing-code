/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashingcode;
import java.util.concurrent.*;
/**
 *
 * @author roman
 */
public class ConcurrentHashMap {
    public static void main(String[] args)
    {
         ConcurrentHashMap<Integer, String> m = 
                   new ConcurrentHashMap<Integer, String>();

        m.put(100, "Jin");
        m.put(101, "Kazuya");
        m.put(102, "Heihachi");
 

        System.out.println("ConcurentHashMap: " + m);

        m.putIfAbsent(101, "Jin");

        System.out.println("\ncurentHashMap: " + m);
 

        m.remove(101, "Kazuya");

        System.out.println("\ncurentHashMap: " + m);
 

        m.replace(100, "Jin", "Heihachi");

        System.out.println("\nConcurentHashMap: " + m);
    }
}

