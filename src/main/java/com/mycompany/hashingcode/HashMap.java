/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashingcode;

import java.util.HashMap;

/**
 *
 * @author roman
 */
public class HashMap {
      public static void main(String[] args) {
    // Create a HashMap object called capitalCities
    HashMap<String, String> capitalCities = new HashMap<String, String>();

    // Add keys and values (Country, City)
    capitalCities.put("Thailand", "Bangkok");
    capitalCities.put("England", "London");
    capitalCities.put("Japan", "Tokyo");
    capitalCities.put("Brazile", "Rio de Janeiro");
    System.out.println(capitalCities);
  }
}

